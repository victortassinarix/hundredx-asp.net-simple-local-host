﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HundredX_ASP.NET_Simple_Local_Host
{
    public class Config
    {
        [JsonIgnore]
        private static string PathOfFile => Environment.CurrentDirectory + Path.DirectorySeparatorChar + "config.json";
        public string ApplicationFolder { get; set; }
        [JsonIgnore]
        public string ApplicationFolderPath => ApplicationFolder.Replace("{CURRENT_DIRECTORY}", Environment.CurrentDirectory);
        public string LocalDomainHost { get; set; }
        public string LocalPortIIS { get; set; }
        public string BrowserStartPath { get; set; }
        public static Config Get()
        {
            if (!File.Exists(PathOfFile))
            {
                throw new Exception("Arquivo de configuração config.json não existe");
            }

            return JsonConvert.DeserializeObject<Config>(File.ReadAllText(PathOfFile));
        }

    }
}
