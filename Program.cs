﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;

namespace HundredX_ASP.NET_Simple_Local_Host
{
    class Program
    {
        #region IMPORTAÇÕES DE DLL EXTERNAS
        [DllImport("kernel32.dll")]
        static extern IntPtr GetConsoleWindow();

        [DllImport("user32.dll")]
        static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

        const int SW_HIDE = 0;
        const int SW_SHOW = 5;
        #endregion

        #region VARIAVEIS DE DEFINIÇÂO DE CAMINHOS DE ARQUIVOS E EXECUTÁVEIS

        public static string PATHIISFOLDERX64 => Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) + Path.DirectorySeparatorChar + "IIS Express" + Path.DirectorySeparatorChar;

        public static string PATHIISFOLDERX32 => Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86) + Path.DirectorySeparatorChar + "IIS Express" + Path.DirectorySeparatorChar;

        public static string PATHIISEXE => (Environment.Is64BitOperatingSystem ? PATHIISFOLDERX64 : PATHIISFOLDERX32) + "iisexpress.exe";

        public static string PATHIISCONFIG => Environment.CurrentDirectory + Path.DirectorySeparatorChar + "applicationhost.config";

        public static string ARGUMENTSIIS => $"/config:\"{PATHIISCONFIG}\"";

        public static string HOSTSFILEPATH => Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.System), "drivers/etc/hosts");

        public static Config Config => _config ?? (_config = Config.Get());
        private static Config _config;

        #endregion 

        static void Main(string[] args)
        {
            Console.WriteLine("Inciando sistema");
            Console.Title = "Iniciando sistema";
            var handle = GetConsoleWindow();
            var p = new Process();

            #region VERIFICANDO A INSTALAÇÃO DO IIS EXPRESS 10
            if (File.Exists(PATHIISEXE))
            {
                Console.WriteLine("Instalação do IIS encontrada com sucesso!");
            }
            else
            {
                var tempFolderIISInstaller = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + Path.DirectorySeparatorChar + "iisexpress10.msi";
                Console.WriteLine("Instalação do IIS não foi encontrada na máquina, preparando instalação...");
                File.WriteAllBytes(tempFolderIISInstaller, Environment.Is64BitOperatingSystem ? EmbedFiles.iisexpress_amd64_pt_BR : EmbedFiles.iisexpress_x86_pt_BR);

                Console.WriteLine("Iniciando instalação do IIS Express 10, por favor aguarde, esta operação pode levar vários minutos");
                Process.Start(tempFolderIISInstaller, "/quiet").WaitForExit();
                Console.WriteLine("IIS Express 10 instalado com sucesso!");
            }
            #endregion

            #region VERIFICANDO E ALTERANDO ENTRADA DE DNS NO ARQUIVO applicationhost.config
            Console.WriteLine("Verificando arquivo applicationhost.config");

            if (!File.Exists(PATHIISCONFIG))
            {
                Console.WriteLine("Criando arquivo applicationhost.config");
                File.WriteAllText(PATHIISCONFIG, EmbedFiles.applicationhost);
            }

            var content = File.ReadAllText(PATHIISCONFIG);
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(PATHIISCONFIG);
            var nodesSite = xmlDoc.GetElementsByTagName("site");
            int i = 1;
            Console.WriteLine("Configurando applicationhost.config ");
            foreach (XmlNode nodeSite in nodesSite)
            {
                nodeSite.Attributes[0].Value = $"{Config.LocalDomainHost}.{i}";
                nodeSite.FirstChild.FirstChild.Attributes[1].Value = Config.ApplicationFolderPath;
                nodeSite.LastChild.FirstChild.Attributes[1].Value = $":{Config.LocalPortIIS}:{ Config.LocalDomainHost}";
                i++;
            }
            xmlDoc.Save(PATHIISCONFIG);
            Console.WriteLine("Configurações salvas com sucesso!");
            #endregion

            #region VERIFICANDO E CRIANDO ENTRADA DE DNS NO ARQUIVO HOSTS
            Console.WriteLine("Verificando arquivo HOSTS");
            bool containsHostsLine = File.ReadAllLines(HOSTSFILEPATH).ToList().Any(x => x == ($"127.0.0.1      {Config.LocalDomainHost}"));
            if (!containsHostsLine)
            {
                Console.WriteLine("Registro não encontrado, preparando para criar");
                using (StreamWriter w = File.AppendText(HOSTSFILEPATH))
                {
                    w.WriteLine($"127.0.0.1      {Config.LocalDomainHost}");
                    Console.WriteLine("Registro de DNS no arquivo HOSTS criado com sucesso!");
                }
            }
            #endregion;

            #region VERIFICA SE HÁ UM PROCESSO DO IIS EXPRESS ABERTO E MATA O PROCESSO
            Console.WriteLine("Verificando processos em execução");
            Process[] processes = Process.GetProcesses();
            foreach (Process process in processes)
            {
                if (process.ProcessName.Contains("iisexpress"))
                {
                    process.Kill();
                    Console.WriteLine($"Processo {process.ProcessName} e finalizado");
                }
            }
            #endregion

            #region CRIANDO E INCIANDO PROCESSO NO WINDOWS
            Console.WriteLine("Inicializando servidor IIS");

            if (!File.Exists(Config.ApplicationFolderPath + Path.DirectorySeparatorChar + "web.config"))
            {
                Console.WriteLine("Não foi possível encontrar o arquivo web.config em {0}", Config.ApplicationFolderPath);
            }
            else
            {

                p.StartInfo = new ProcessStartInfo()
                {
                    FileName = PATHIISEXE,
                    Arguments = ARGUMENTSIIS,
                    UseShellExecute = false,
                    RedirectStandardOutput = true,
                    RedirectStandardError = true,
                    CreateNoWindow = false,
                    WindowStyle = ProcessWindowStyle.Hidden,
                };
                p.Start();

                var reader = p.StandardOutput;
                while (!reader.EndOfStream)
                {
                    var nextLine = reader.ReadLine();
                    if (nextLine == "IIS Express is running.")
                    {
                        Console.Write($"Servidor iniciado com sucesso");
                        if (Config.BrowserStartPath != null)
                        {
                            Console.Write($", abrindo navegador em http://{Config.LocalDomainHost}:{Config.LocalPortIIS}");
                            Process.Start(Config.BrowserStartPath, $"http://{Config.LocalDomainHost}:{Config.LocalPortIIS}");
                        }

                        Thread.Sleep(2000);
                        ShowWindow(handle, SW_HIDE);
                    }
                }

                p.WaitForExit();
                #endregion
            }

            Console.ReadKey();
        }
    }
}
